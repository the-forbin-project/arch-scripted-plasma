#!/bin/bash
# Written by Palanthis
set -e
 
yaourt -S --needed --noconfirm ckb-next

sudo systemctl enable ckb-next-daemon
sudo systemctl start ckb-next-daemon
