#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "Ensuring mirrorlist is up-to-date"
sudo reflector --country 'United States' --age 24 --protocol http --sort rate --save /etc/pacman.d/mirrorlist
echo "Done!"
