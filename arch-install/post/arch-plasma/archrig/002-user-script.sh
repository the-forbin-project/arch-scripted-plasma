#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################

# Add lines to .bashrc
echo 'export PATH="/usr/lib/ccache/bin/:$PATH"' >> ~/.bashrc
echo 'export MAKEFLAGS="-j5 -l4"' >> ~/.bashrc

# Xorg Core
sudo pacman -S xorg-server xorg-apps xorg-xinit xorg-twm xorg-xclock xterm --noconfirm --needed
sudo pacman -S linux-headers --noconfirm --needed

# Uncomment the below line for NVidia Drivers
sudo pacman -S nvidia-dkms nvidia-settings nvidia-utils lib32-nvidia-utils --noconfirm --needed
sudo pacman -S lib32-opencl-nvidia opencl-nvidia libvdpau lib32-libvdpau --noconfirm --needed
sudo pacman -S libxnvctrl --noconfirm --needed

# Optional WiFi Drivers
sudo pacman -S broadcom-wl-dkms --noconfirm --needed

# Call common script
sh ../003-common-script.sh
