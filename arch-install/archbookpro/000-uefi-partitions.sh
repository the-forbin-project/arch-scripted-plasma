#!/bin/bash
set -e
#
##########################################################
# Author 	: 	Palanthis (palanthis@gmail.com)
# Website 	: 	http://github.com/Palanthis
# License	:	Distributed under the terms of GNU GPL v3
# Warning	:	These scripts come with NO WARRANTY!!!!!!
##########################################################
echo
echo "WARNING!!! This script will *destroy everything* on /dev/sda"
echo "Do NOT use this script if you need to do"
echo "manual partitioning!"
echo
read -n 1 -s -r -p "Press Enter to continue or Ctrl+C to quit."

sfdisk /dev/sda < sda-uefi.sfdisk

read -n 1 -s -r -p "/dev/sda done. Press Enter to continue or Ctrl+C to quit."

fdisk -l | grep /dev/sd

echo
echo "Do these partitions look okay?"
echo
read -n 1 -s -r -p "Press Enter to continue."


